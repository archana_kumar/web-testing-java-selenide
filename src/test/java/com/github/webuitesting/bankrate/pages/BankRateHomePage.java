package com.github.webuitesting.bankrate.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;


public class BankRateHomePage {
    @FindBy (css="div#mobileHide div.first > div > ul#compare_rates_menu > li:nth-child(1) > input")
    private SelenideElement radioButton;

    @FindBy (css = "div#mortgage li:nth-child(3) > input")
    private SelenideElement productRadioButton;

    @FindBy (css = "div#mortgage > a")
    private SelenideElement nextButton;

    public void chooseAndNavigate(){
        radioButton.click();
        productRadioButton.click();
        nextButton.click();

    }



}
