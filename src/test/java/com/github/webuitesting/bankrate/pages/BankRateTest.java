package com.github.webuitesting.bankrate.pages;


import com.github.webuitesting.BaseTest;
import com.github.webuitesting.nbastore.pages.NbaStoreHomePage;
import com.relevantcodes.extentreports.ExtentTest;
import org.junit.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class BankRateTest extends BaseTest{

    @Test
    public void testSelectAProduct() throws InterruptedException {

        ExtentTest extentTest = extent.startTest("Bankrate homepage product selection");
        String baseUrl = config.getString("bankrate.baseUrl");
        logger.info("Now starting the test to test the URL {}", () -> baseUrl);
        BankRateHomePage homePage = open(baseUrl, BankRateHomePage.class);
       // $("div#mobileHide div.first > div > ul#compare_rates_menu > li:nth-child(1) > input").click();
      //  $("div#mortgage li:nth-child(3) > input").click();
        //$("div#mortgage > a").click();
        homePage.chooseAndNavigate();
        //TODO  - next steps

    }
}
