package com.github.webuitesting.nbastore;


import com.github.webuitesting.BaseTest;
import com.github.webuitesting.nbastore.pages.NbaStoreHomePage;
import com.github.webuitesting.nbastore.pages.NbaStoreSearchResults;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Test;

import static com.codeborne.selenide.Selenide.open;


public class NbaTest extends BaseTest {
    @Test
    public void testBasicSearch() throws InterruptedException {

        ExtentTest extentTest = extent.startTest("NBA homepage search");
        String baseUrl = config.getString("nba.baseUrl");
        logger.info("Now starting the test to test the URL {}", () -> baseUrl);
        NbaStoreHomePage page = open(baseUrl, NbaStoreHomePage.class);
        String searchTerm = "Shoes";
        extentTest.log(LogStatus.INFO, "Searching for term" + searchTerm);
        NbaStoreSearchResults results = page.searchFor("Shoes");
        results.verifySelectionText("Footwear");

        extentTest.log(LogStatus.PASS, "Search Returned   " + results.getResults().get(0));
        extent.endTest(extentTest);
    }


}
