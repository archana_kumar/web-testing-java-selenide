package com.github.webuitesting.nbastore.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Condition.text;


public class NbaStoreSearchResults {

    @FindBy(css = "#searchquery")
    private ElementsCollection results;

    @FindBy(css = "div#filterdepartment span.filterValue")
    private SelenideElement yourSelections;


    public void verifySelectionText(String expectedText) {
        yourSelections.shouldHave(text(expectedText));

    }

    public ElementsCollection getResults() {
        return results;
    }


}


