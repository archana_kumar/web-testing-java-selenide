package com.github.webuitesting.nbastore.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;


public class NbaStoreHomePage {
    @FindBy(css = "input#searchquery")
    private SelenideElement searchInput;

    public NbaStoreSearchResults searchFor(String text) {
        searchInput.val(text).pressEnter();
        return page(NbaStoreSearchResults.class);
    }


}